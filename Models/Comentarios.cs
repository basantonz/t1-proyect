﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aplicacion_de_prueba.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public string Autor { get; set; }
        public string Contenido { get; set; }
        public string Fecha { get; set; }

        public string Imagen { get; set; }

        public static List<Comentario> GetComentarios()
        {
            return new List<Comentario>()
            {
               new Comentario {Id=1, Autor="Pedrito",Contenido="Buen post",Fecha="25/12/20"},
               new Comentario {Id=2, Autor="Raúl",Contenido="Información muy interesante",Fecha="24/12/20"}
            };
        }
    }
}
