﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aplicacion_de_prueba.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public DateTime Fecha { get; set; }
        public string Contenido { get; set; }

        public string Imagen { get; set; }

        public static List<Post> GetPosts()
        {
            return new List<Post>()
            {
               new Post {Id=1,Titulo="33 predicciones futuristas hechas en el pasado: erróneas, acertadas y disparatadas",Autor="Manuel Mendoza Perez",Fecha=DateTime.Now,Contenido="La tecnología avanza de una forma impresionante, lo que hoy es lo más avanzado e innovador en unos años se convierte en obsoleto, y ya no digamos en unos años, en unos cuantos meses para el caso de algunos dispositivos. Al estar dentro de estos temas todos los días, tal vez podemos olvidar cómo ha sido esta evolución, o bien, perder la capacidad de asombro, por ello las cosas que hablan del futuro suelen entusiasmarnos, aunque se trate de prototipos o ideas, ya que nos hace imaginar cómo será la vida dentro de algunos años.", Imagen="https://i.blogs.es/f4bd95/dfgssdg/1366_2000.jpg" },
                new Post {Id=2,Titulo="De folios rojos a escudos anti-drones: así se defiende la industria audiovisual de las indeseables filtraciones",Autor="Juan Alcántara Villanueva",Fecha=DateTime.Now,Contenido="El imparable progreso de la red de redes durante la última década y las posibilidades que brinda actualmente —para bien o para mal— en cuanto al manejo y la difusión de datos de todo tipo se refiere, se erige como un arma de doble filo en lo que respecta a su cada vez más estrecha relación con la industria cinematográfica y televisiva.En contraposición a los revolucionarios avances en los modos de consumo audiovisual, abanderados por el ascenso de las plataformas de video on demand, y a las ventajas para los creativos de todo el mundo a la hora de dar a conocer su obra, internet —o más bien parte de sus usuarios— está mostrando su cara más oscura frente a la propiedad intelectual, ya no como ente maligno per se, sino como una herramienta que, de caer en malas manos, puede ser empleada para todo tipo de causas de dudosa ética y legalidad.", Imagen="https://i.blogs.es/f35ee3/guion-episodio-vii/1366_2000.jpg" }
           };
        }
    }
}
