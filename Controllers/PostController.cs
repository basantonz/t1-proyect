﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Aplicacion_de_prueba.Models;

namespace Aplicacion_de_prueba.Controllers
{
    public class PostController : Controller
    {
        [HttpGet]
        public ViewResult Detalle(int id)
        {
            Post post = null;
            var posts = Post.GetPosts();
            foreach(var item in posts)
            {
                if (item.Id == id)
                    post = item;
            }
            return View(post);
        }
    }
}

